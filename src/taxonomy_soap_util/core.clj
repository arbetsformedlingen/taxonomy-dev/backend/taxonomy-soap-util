(ns taxonomy-soap-util.core
  (:require
   [clj-http.client :as client]
   [paos.service :as service]
   [paos.wsdl :as wsdl]
   [clojure.string :as st])
  (:gen-class))

;; "GetAllSUNField3" funkar ej 4,6,7,9, 10,13,16,21,23,26,28,
(def all-easy-operations
  ["GetAllMunicipalities"
   "GetAllWorkTimeExtents"

   "GetAllEducationFields"

   "GetAllSkillHeadlines"


   "GetAllLanguageLevels"


   "GetAllDrivingLicences"
   "GetAllJobSituations"

   "GetAllPostLocalities"
   "GetAllCountries"

   "GetAllOccupationNames"
   "GetAllEmploymentDurations"
   "GetAllUnemploymentBenefitSocieties"
   "GetAllMunicipalityHomePages"

   "GetAllEmploymentTypes"

   "GetAllOccupationNameSynonyms"
   "GetAllLanguages"

   "GetAllLocaleFields"


  ;; "GetAllAFOffices"
   "GetAllWageTypes"
   "GetAllSUNLevel1"
   "GetAllSUNLevel2"
   "GetAllBSKOccupationNames"
   "GetAllSkills"
   "GetAllLocaleGroups"
   "GetAllEducationLevels"
   "GetAllAIDOccupationNames"
   "GetAllSkillMainHeadlines"
   "GetAllContinents"
   "GetAllLocaleGroupSkills"

   "GetAllBEFOccupationNames"
   "GetAllEURegions"
   ])

(def more-operations ["GetAllLocaleFields"


           ;; "GetAllAFOffices"
           "GetAllWageTypes"

           "GetAllSUNLevel1"
           "GetAllSUNLevel2"

           "GetAllBSKOccupationNames"
           "GetAllSkills"
           "GetAllLocaleGroups"

                      "GetAllEducationLevels"
           "GetAllAIDOccupationNames"
           "GetAllSkillMainHeadlines"
           "GetAllContinents"
           "GetAllLocaleGroupSkills"

           "GetAllBEFOccupationNames"
           "GetAllEURegions"])

(def all-hard-operations
  ["GetAllSUNField3"
   "GetAllLanguagesWithISO"
   "GetAllSNILevel1"
   "GetAllSNILevel2"
   "GetAllSUNField2"
   "GetAllExperiencesLast"
   "GetAllSUNField1"
   "GetAllOccupationNamesVersion"
   "GetAllSUNLevel3"
   "GetAllLocaleLevel3"
   "GetAllSkillsVersion"

   "GetAllExperiencesYear"
   "GetAllInternalJobSituations"
   "GetAllPostCodes"
   ]
  )


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(def taxonomy-service-url "http://api.arbetsformedlingen.se/taxonomi/v0/TaxonomiService.asmx?wsdl")

(defn get-all-occupation-names []
  (let [soap-service (wsdl/parse taxonomy-service-url)
        srv          (get-in soap-service ["TaxonomiServiceSoap12" :operations "GetAllOccupationNames"])
        soap-url     "http://api.arbetsformedlingen.se/taxonomi/v0/TaxonomiService.asmx"
        content-type (service/content-type srv)
        headers      (service/soap-headers srv)
        mapping      (service/request-mapping srv)
        context      (assoc-in mapping ["Envelope" "Body" "GetAllOccupationNames" "languageId" :__value] "502")
        body         (service/wrap-body srv context)
        parse-fn     (partial service/parse-response srv)]
    (-> soap-url
        (client/post {:content-type content-type
                      :body         body
                      :headers      headers})
        :body
        parse-fn
        #_identity
        (get-in ["Envelope" "Body" "GetAllOccupationNamesResponse" "GetAllOccupationNamesResult" "OccupationNames"]))))

(defn occupation-name-to-java-code [occupation-name]
  (let [id (get-in occupation-name ["OccupationName" "OccupationNameID" "__value"])
        term (get-in occupation-name ["OccupationName" "Term" "__value"])
        locale-code (get-in occupation-name ["OccupationName" "LocaleCode" "__value"])]
    (format
     "%s;%s;%s"
     id term locale-code)))

(defn write-csv-to-file
  "Write a lazy seq to a file"
  [seq class-name]
  (->>
   seq
   (interpose \newline)
   (apply str)
   (spit (str  "gen/csv/" class-name ".csv"))))





(defn get-all-operations []
  (filter (fn [s] (clojure.string/starts-with? s "GetAll")) (map first (get-in  (wsdl/parse taxonomy-service-url) ["TaxonomiServiceSoap12" :operations]))))

(defn get-soap-data [operation-name]
  (let [soap-service (wsdl/parse taxonomy-service-url)
        srv          (get-in soap-service ["TaxonomiServiceSoap12" :operations operation-name])
        soap-url     "http://api.arbetsformedlingen.se/taxonomi/v0/TaxonomiService.asmx"
        content-type (service/content-type srv)
        headers      (service/soap-headers srv)
        mapping      (service/request-mapping srv)
        context      (assoc-in mapping ["Envelope" "Body" operation-name "languageId" :__value] "502")
        body         (service/wrap-body srv context)
        parse-fn     (partial service/parse-response srv)]
    (-> soap-url
        (client/post {:content-type content-type
                      :body         body
                      :headers      headers})
        :body
        parse-fn
        #_identity
        (get-in ["Envelope" "Body" (str operation-name "Response") (str operation-name "Result")  (last (clojure.string/split operation-name #"GetAll"))]))))

;; (first (get-soap-data (nth (get-all-operations) 17)))
(def test-municipality {"Municipality"
                        {"MunicipalityID" {"__value" "253"},
                         "Term" {"__value" "Ale"},
                         "NationalNUTSLAU2Code" {"__value" "1440"},
                         "NationalNUTSLevel3Code" {"__value" "14"}}})

(def s
  "o.setMunicipalityID(Integer.valueOf(split[0]));
                o.setTerm(split[1]);
                o.setNationalNUTSLAU2Code(split[2]);
                o.setNationalNUTSLevel3Code(split[3]);
")

(defn generate-setters [properties]
  (map-indexed (fn [i element]
                 (if (= "SortOrder" element)
                   (format "o.set%1$s(Integer.valueOf(split[%2$s])); \n"  element (+ 1 i))
                   (format "o.set%1$s(split[%2$s]); \n"  element (+ 1 i))
                   )
                 )
               properties))

(defn get-csv-data [soap-element]
  (clojure.string/join ";" (map #(get % "__value") (vals (first (vals soap-element))))))

;; TODO change name to ENDpoint code
(defn write-endpoint-code [repository-code operation-class]
  (spit (str "gen/" operation-class "Endpoint.java") repository-code))

(defn de-capitalize [s]
  (do
    (clojure.pprint/pprint s)
    (str (st/lower-case (first s)) (apply str (rest s)))))

(defn create-endpoint-code [class-name
                            operation-name]
  (let [class-name-lowercase (de-capitalize class-name)
        operation-names-lowercase (de-capitalize operation-name)]

    (format (slurp "resources/template2.java")
            class-name
            class-name-lowercase
            operation-name
            operation-names-lowercase)))

(defn write-javaclass [java-code class-name]
  (spit (str "gen/" class-name "Repository.java") java-code))

(defn generate-code-from-operation [operation-name]
  (let [soap-data (get-soap-data operation-name)

        operation-result-single-element (first soap-data)

        operation-class-plural (last (clojure.string/split operation-name #"GetAll"))

        operation-class (first (keys operation-result-single-element))
        class-properties (keys (first (vals operation-result-single-element)))
        setIdString (format "o.set%1$s(Integer.valueOf(split[0])); \n" (first class-properties))
        setters (apply str (generate-setters (rest class-properties)))
        all-setters (str setIdString setters)
        java-class-code (format (slurp "resources/template.java") operation-class operation-class-plural all-setters)

        endpoint-code (create-endpoint-code operation-class operation-name)

        csv-data (map get-csv-data soap-data)
        ]
    {
     :operation-name operation-name
     :operation-class operation-class
     :java-code java-class-code
     :endpoint-code endpoint-code
     :csv csv-data
     }
    ))

(defn save-generated-code-to-disk [{:keys [operation-name
                                           operation-class
                                           java-code
                                           endpoint-code
                                           csv
                                           ]}]

  (do
    (write-csv-to-file csv operation-class)
    (write-javaclass java-code operation-class)
    (write-endpoint-code endpoint-code operation-class)
    ))


;; (format (slurp "resources/template.java") "Municipality" "Municipalities" s)
