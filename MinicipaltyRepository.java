package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class MunicipalityRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllMunicipalitiesResponse response = new GetAllMunicipalitiesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfMunicipality array = new ArrayOfMunicipality();
        response.setGetAllMunicipalitiesResult(array);

        Resource file = resourceLoader.getResource("classpath:Municipality.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                Municipality o = new Municipality();

                o.setMunicipalityID(Integer.valueOf(split[0]));
                o.setTerm(split[1]);
                o.setNationalNUTSLAU2Code(split[2]);
                o.setNationalNUTSLevel3Code(split[3]);


                array.getMunicipality().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
