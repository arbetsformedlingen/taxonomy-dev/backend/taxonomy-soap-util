package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllSkillMainHeadlines;
        import se.arbetsformedlingen.wsdl.GetAllSkillMainHeadlinesResponse;

@Endpoint
public class SkillMainHeadlineEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private SkillMainHeadlineRepository skillMainHeadlineRepository;

    @Autowired
    public SkillMainHeadlineEndpoint(SkillMainHeadlineRepository skillMainHeadlineRepository) {
        this.skillMainHeadlineRepository = skillMainHeadlineRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllSkillMainHeadlines")
    @ResponsePayload
    public GetAllSkillMainHeadlinesResponse getAllSkillMainHeadlines(@RequestPayload GetAllSkillMainHeadlines request) {
        return skillMainHeadlineRepository.getAllSkillMainHeadlines();
    }

}
