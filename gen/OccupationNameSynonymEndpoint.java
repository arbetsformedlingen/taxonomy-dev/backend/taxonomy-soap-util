package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllOccupationNameSynonyms;
        import se.arbetsformedlingen.wsdl.GetAllOccupationNameSynonymsResponse;

@Endpoint
public class OccupationNameSynonymEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private OccupationNameSynonymRepository occupationNameSynonymRepository;

    @Autowired
    public OccupationNameSynonymEndpoint(OccupationNameSynonymRepository occupationNameSynonymRepository) {
        this.occupationNameSynonymRepository = occupationNameSynonymRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllOccupationNameSynonyms")
    @ResponsePayload
    public GetAllOccupationNameSynonymsResponse getAllOccupationNameSynonyms(@RequestPayload GetAllOccupationNameSynonyms request) {
        return occupationNameSynonymRepository.getAllOccupationNameSynonyms();
    }

}
