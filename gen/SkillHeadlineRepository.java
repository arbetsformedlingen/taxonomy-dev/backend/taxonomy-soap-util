package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class SkillHeadlineRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllSkillHeadlinesResponse response = new GetAllSkillHeadlinesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfSkillHeadline array = new ArrayOfSkillHeadline();
        response.setGetAllSkillHeadlinesResult(array);

        Resource file = resourceLoader.getResource("classpath:SkillHeadline.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                SkillHeadline o = new SkillHeadline();

                o.setSkillMainHeadline(Integer.valueOf(split[0])); 
o.setSkillHeadlineID(split[1]); 
o.setTerm(split[2]); 


                array.getSkillHeadline().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllSkillHeadlinesResponse getAllSkillHeadlines() {
        return response;
    }
}
