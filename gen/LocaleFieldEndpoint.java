package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllLocaleFields;
        import se.arbetsformedlingen.wsdl.GetAllLocaleFieldsResponse;

@Endpoint
public class LocaleFieldEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private LocaleFieldRepository localeFieldRepository;

    @Autowired
    public LocaleFieldEndpoint(LocaleFieldRepository localeFieldRepository) {
        this.localeFieldRepository = localeFieldRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllLocaleFields")
    @ResponsePayload
    public GetAllLocaleFieldsResponse getAllLocaleFields(@RequestPayload GetAllLocaleFields request) {
        return localeFieldRepository.getAllLocaleFields();
    }

}
