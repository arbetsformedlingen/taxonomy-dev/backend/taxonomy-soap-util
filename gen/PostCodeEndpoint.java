package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllPostCodes;
        import se.arbetsformedlingen.wsdl.GetAllPostCodesResponse;

@Endpoint
public class PostCodeEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private PostCodeRepository postCodeRepository;

    @Autowired
    public PostCodeEndpoint(PostCodeRepository postCodeRepository) {
        this.postCodeRepository = postCodeRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllPostCodes")
    @ResponsePayload
    public GetAllPostCodesResponse getAllPostCodes(@RequestPayload GetAllPostCodes request) {
        return postCodeRepository.getAllPostCodes();
    }

}
