package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class LanguageRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllLanguagesResponse response = new GetAllLanguagesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfLanguage array = new ArrayOfLanguage();
        response.setGetAllLanguagesResult(array);

        Resource file = resourceLoader.getResource("classpath:Language.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                Language o = new Language();

                o.setLanguageID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 


                array.getLanguage().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllLanguagesResponse getAllLanguages() {
        return response;
    }
}
