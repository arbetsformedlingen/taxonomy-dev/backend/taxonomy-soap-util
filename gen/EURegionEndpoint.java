package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllEURegions;
        import se.arbetsformedlingen.wsdl.GetAllEURegionsResponse;

@Endpoint
public class EURegionEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private EURegionRepository eURegionRepository;

    @Autowired
    public EURegionEndpoint(EURegionRepository eURegionRepository) {
        this.eURegionRepository = eURegionRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllEURegions")
    @ResponsePayload
    public GetAllEURegionsResponse getAllEURegions(@RequestPayload GetAllEURegions request) {
        return eURegionRepository.getAllEURegions();
    }

}
