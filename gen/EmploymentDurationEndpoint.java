package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllEmploymentDurations;
        import se.arbetsformedlingen.wsdl.GetAllEmploymentDurationsResponse;

@Endpoint
public class EmploymentDurationEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private EmploymentDurationRepository employmentDurationRepository;

    @Autowired
    public EmploymentDurationEndpoint(EmploymentDurationRepository employmentDurationRepository) {
        this.employmentDurationRepository = employmentDurationRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllEmploymentDurations")
    @ResponsePayload
    public GetAllEmploymentDurationsResponse getAllEmploymentDurations(@RequestPayload GetAllEmploymentDurations request) {
        return employmentDurationRepository.getAllEmploymentDurations();
    }

}
