package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class WorkTimeExtentRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllWorkTimeExtentsResponse response = new GetAllWorkTimeExtentsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfWorkTimeExtent array = new ArrayOfWorkTimeExtent();
        response.setGetAllWorkTimeExtentsResult(array);

        Resource file = resourceLoader.getResource("classpath:WorkTimeExtent.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                WorkTimeExtent o = new WorkTimeExtent();

                o.setWorkTimeExtentID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setSortOrder(Integer.valueOf(split[2])); 


                array.getWorkTimeExtent().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllWorkTimeExtentsResponse getAllWorkTimeExtents() {
        return response;
    }
}
