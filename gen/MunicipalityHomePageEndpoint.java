package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllMunicipalityHomePages;
        import se.arbetsformedlingen.wsdl.GetAllMunicipalityHomePagesResponse;

@Endpoint
public class MunicipalityHomePageEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private MunicipalityHomePageRepository municipalityHomePageRepository;

    @Autowired
    public MunicipalityHomePageEndpoint(MunicipalityHomePageRepository municipalityHomePageRepository) {
        this.municipalityHomePageRepository = municipalityHomePageRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllMunicipalityHomePages")
    @ResponsePayload
    public GetAllMunicipalityHomePagesResponse getAllMunicipalityHomePages(@RequestPayload GetAllMunicipalityHomePages request) {
        return municipalityHomePageRepository.getAllMunicipalityHomePages();
    }

}
