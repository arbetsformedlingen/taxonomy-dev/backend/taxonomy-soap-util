package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllAIDOccupationNames;
        import se.arbetsformedlingen.wsdl.GetAllAIDOccupationNamesResponse;

@Endpoint
public class AIDOccupationNameEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private AIDOccupationNameRepository aIDOccupationNameRepository;

    @Autowired
    public AIDOccupationNameEndpoint(AIDOccupationNameRepository aIDOccupationNameRepository) {
        this.aIDOccupationNameRepository = aIDOccupationNameRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllAIDOccupationNames")
    @ResponsePayload
    public GetAllAIDOccupationNamesResponse getAllAIDOccupationNames(@RequestPayload GetAllAIDOccupationNames request) {
        return aIDOccupationNameRepository.getAllAIDOccupationNames();
    }

}
