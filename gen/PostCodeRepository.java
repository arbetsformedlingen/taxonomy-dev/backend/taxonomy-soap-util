package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class PostCodeRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllPostCodesResponse response = new GetAllPostCodesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfPostCode array = new ArrayOfPostCode();
        response.setGetAllPostCodesResult(array);

        Resource file = resourceLoader.getResource("classpath:PostCode.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                PostCode o = new PostCode();

                o.setCode(Integer.valueOf(split[0])); 
o.setPostLocalityID(split[1]); 
o.setPostLocality(split[2]); 
o.setNationalNUTSLAU2Code(split[3]); 
o.setKeyAccount(split[4]); 


                array.getPostCode().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllPostCodesResponse getAllPostCodes() {
        return response;
    }
}
