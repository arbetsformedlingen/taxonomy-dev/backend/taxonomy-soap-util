package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class EducationLevelRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllEducationLevelsResponse response = new GetAllEducationLevelsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfEducationLevel array = new ArrayOfEducationLevel();
        response.setGetAllEducationLevelsResult(array);

        Resource file = resourceLoader.getResource("classpath:EducationLevel.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                EducationLevel o = new EducationLevel();

                o.setEducationLevelID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setDescription(split[2]); 


                array.getEducationLevel().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllEducationLevelsResponse getAllEducationLevels() {
        return response;
    }
}
