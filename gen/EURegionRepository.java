package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class EURegionRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllEURegionsResponse response = new GetAllEURegionsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfEURegion array = new ArrayOfEURegion();
        response.setGetAllEURegionsResult(array);

        Resource file = resourceLoader.getResource("classpath:EURegion.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                EURegion o = new EURegion();

                o.setEURegionID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setNationalNUTSLevel3Code(split[2]); 


                array.getEURegion().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllEURegionsResponse getAllEURegions() {
        return response;
    }
}
