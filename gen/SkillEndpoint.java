package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllSkills;
        import se.arbetsformedlingen.wsdl.GetAllSkillsResponse;

@Endpoint
public class SkillEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private SkillRepository skillRepository;

    @Autowired
    public SkillEndpoint(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllSkills")
    @ResponsePayload
    public GetAllSkillsResponse getAllSkills(@RequestPayload GetAllSkills request) {
        return skillRepository.getAllSkills();
    }

}
