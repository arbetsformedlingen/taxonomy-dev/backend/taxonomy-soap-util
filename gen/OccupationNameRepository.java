package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class OccupationNameRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllOccupationNamesResponse response = new GetAllOccupationNamesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfOccupationName array = new ArrayOfOccupationName();
        response.setGetAllOccupationNamesResult(array);

        Resource file = resourceLoader.getResource("classpath:OccupationName.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                OccupationName o = new OccupationName();

                o.setOccupationNameID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setLocaleCode(split[2]); 


                array.getOccupationName().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllOccupationNamesResponse getAllOccupationNames() {
        return response;
    }
}
