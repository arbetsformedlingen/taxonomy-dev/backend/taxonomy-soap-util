package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class WageTypeRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllWageTypesResponse response = new GetAllWageTypesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfWageType array = new ArrayOfWageType();
        response.setGetAllWageTypesResult(array);

        Resource file = resourceLoader.getResource("classpath:WageType.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                WageType o = new WageType();

                o.setWageTypeID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setSortOrder(Integer.valueOf(split[2])); 


                array.getWageType().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllWageTypesResponse getAllWageTypes() {
        return response;
    }
}
