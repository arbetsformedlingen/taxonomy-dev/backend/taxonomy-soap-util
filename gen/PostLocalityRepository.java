package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class PostLocalityRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllPostLocalitiesResponse response = new GetAllPostLocalitiesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfPostLocality array = new ArrayOfPostLocality();
        response.setGetAllPostLocalitiesResult(array);

        Resource file = resourceLoader.getResource("classpath:PostLocality.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                PostLocality o = new PostLocality();

                o.setPostLocalityID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 


                array.getPostLocality().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllPostLocalitiesResponse getAllPostLocalities() {
        return response;
    }
}
