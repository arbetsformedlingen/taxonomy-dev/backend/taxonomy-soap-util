package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class OccupationNameSynonymRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllOccupationNameSynonymsResponse response = new GetAllOccupationNameSynonymsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfOccupationNameSynonym array = new ArrayOfOccupationNameSynonym();
        response.setGetAllOccupationNameSynonymsResult(array);

        Resource file = resourceLoader.getResource("classpath:OccupationNameSynonym.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                OccupationNameSynonym o = new OccupationNameSynonym();

                o.setPopularSynonymID(Integer.valueOf(split[0])); 
o.setSynonymTerm(split[1]); 
o.setOccupationNameID(split[2]); 
o.setOccupationNameTerm(split[3]); 


                array.getOccupationNameSynonym().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllOccupationNameSynonymsResponse getAllOccupationNameSynonyms() {
        return response;
    }
}
