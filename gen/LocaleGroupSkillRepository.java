package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class LocaleGroupSkillRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllLocaleGroupSkillsResponse response = new GetAllLocaleGroupSkillsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfLocaleGroupSkill array = new ArrayOfLocaleGroupSkill();
        response.setGetAllLocaleGroupSkillsResult(array);

        Resource file = resourceLoader.getResource("classpath:LocaleGroupSkill.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                LocaleGroupSkill o = new LocaleGroupSkill();

                o.setLocaleCode(Integer.valueOf(split[0])); 
o.setSkillID(split[1]); 


                array.getLocaleGroupSkill().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllLocaleGroupSkillsResponse getAllLocaleGroupSkills() {
        return response;
    }
}
