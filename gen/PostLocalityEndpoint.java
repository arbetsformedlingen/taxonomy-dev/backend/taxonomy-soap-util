package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllPostLocalities;
        import se.arbetsformedlingen.wsdl.GetAllPostLocalitiesResponse;

@Endpoint
public class PostLocalityEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private PostLocalityRepository postLocalityRepository;

    @Autowired
    public PostLocalityEndpoint(PostLocalityRepository postLocalityRepository) {
        this.postLocalityRepository = postLocalityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllPostLocalities")
    @ResponsePayload
    public GetAllPostLocalitiesResponse getAllPostLocalities(@RequestPayload GetAllPostLocalities request) {
        return postLocalityRepository.getAllPostLocalities();
    }

}
