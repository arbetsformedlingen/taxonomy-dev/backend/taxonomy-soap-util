package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class CountryRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllCountriesResponse response = new GetAllCountriesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfCountry array = new ArrayOfCountry();
        response.setGetAllCountriesResult(array);

        Resource file = resourceLoader.getResource("classpath:Country.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                Country o = new Country();

                o.setCountryID(Integer.valueOf(split[0])); 
o.setCountryCode(split[1]); 
o.setCountryCodeAlpha3(split[2]); 
o.setTerm(split[3]); 
o.setContinentID(split[4]); 


                array.getCountry().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllCountriesResponse getAllCountries() {
        return response;
    }
}
