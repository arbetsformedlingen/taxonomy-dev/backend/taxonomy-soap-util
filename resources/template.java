package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class %1$sRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAll%2$sResponse response = new GetAll%2$sResponse();


    @PostConstruct
    public void initData() {

        ArrayOf%1$s array = new ArrayOf%1$s();
        response.setGetAll%2$sResult(array);

        Resource file = resourceLoader.getResource("classpath:%1$s.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                %1$s o = new %1$s();

                %3$s

                array.get%1$s().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAll%2$sResponse getAll%2$s() {
        return response;
    }
}
