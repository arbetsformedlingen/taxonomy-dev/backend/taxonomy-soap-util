package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.%3$s;
        import se.arbetsformedlingen.wsdl.%3$sResponse;

@Endpoint
public class %1$sEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private %1$sRepository %2$sRepository;

    @Autowired
    public %1$sEndpoint(%1$sRepository %2$sRepository) {
        this.%2$sRepository = %2$sRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "%3$s")
    @ResponsePayload
    public %3$sResponse %4$s(@RequestPayload %3$s request) {
        return %2$sRepository.%4$s();
    }

}
